/*
 * Copyright (c) 2022, Seqera Labs.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * This Source Code Form is "Incompatible With Secondary Licenses", as
 * defined by the Mozilla Public License, v. 2.0.
 *
 */
manifest {
    description = 'Proof-of-concept pipeline for performing hyperparameter optimization of machine learning models with Nextflow'
    author = 'Ben Sherman'
    nextflowVersion = '>=22.10.0'
}

nextflow {
    enable.moduleBinaries = true
}

/*
 * Default pipeline parameters. They can be overriden on the command line eg.
 * given `params.foo` specify on the run command line `--foo some_value`.
 */
params {
    dataloc = 's3://nextflow-bucket-s3/data/'
    datadir = '/qfs/projects/chess/leeh736/autobahn-em/patch/data'
    outdir = 'results'
}

plugins {
    id 'nf-amazon'
}

docker {
    /*runOptions = "--mount type=bind,source=/qfs/projects/chess/leeh736/autobahn-em,target=/home/ec2-user"
     */
    enabled = true
}

aws {
  region = 'us-west-2'
  /*client {
   *   uploadChunkSize = 10485760
   *}
   */
   batch {
      volumes = '/scratch/fusion:/tmp'
      executionRole = 'arn:aws:iam::622093707179:role/ecsInstanceRole'
   }
  /*batch{
   *   cliPath = '/usr/local/bin/aws'
   *}
   */
}

process {
    withLabel: 'short' {
        executor = 'slurm'
        clusterOptions= "-A chess"
        conda = '/qfs/projects/chess/leeh736/autobahn-em/conda.yml'
    }
    withLabel: 'long' {
        executor = 'awsbatch'
        containerOptions = "--runtime=nvidia --gpus all --privileged"
        queue = 'GPU-Ubuntu-Queue'
        container = 'docker.io/oceanebel2/chess:latest'
        /* container = '763104351884.dkr.ecr.us-east-1.amazonaws.com/pytorch-training:2.2.0-gpu-py310-cu121-ubuntu20.04-ec2'
         */
        errorStrategy = 'retry'
        maxRetries    = 3
        memory  = 65.GB
    }
    withLabel: 'long-spot' {
        executor = 'awsbatch'
        containerOptions = "--runtime=nvidia --gpus all --privileged"
        queue = 'GPU-Spot-Ubuntu-Queue'
        container = 'docker.io/oceanebel2/chess:latest'
        errorStrategy = 'retry'
        maxRetries    = 3
        memory  = 65.GB
    }
    withLabel: 'local_launch' {
        executor = 'local'
    }
}

aws.client.maxConnections = 4
aws.batch.maxParallelTransfers = 8

/*
 * Execution profiles for different environments.
 */

profiles {
    slurm {
       workDir = '/qfs/projects/chess/leeh736/autobahn-em/work'
    }

    conda {
        process.conda = "/qfs/projects/chess/leeh736/autobahn-em/conda.yml"
    }

    aws_batch {
        workDir = 's3://nextflow-bucket-s3/scratch'
    }

}
