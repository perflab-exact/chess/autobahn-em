import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from get_input import input_data
from get_output import output_data
from MLE2 import MLE
import numpy as np
import statistics
import math
from scipy.spatial import distance

def get_relative_error(pred, targ):
    # This function will calculate the relative error between target and predicted values
    RE = []
    for i in range(len(pred)):
        #print(targ[i])
        if targ[i] == 0:
            RE.append(abs(pred[i]-targ[i]))
        else:
            RE.append((abs(pred[i]-targ[i]))/targ[i])
    return RE

in_ = input_data()
out_ = output_data() 
mle = MLE()
#out.csv sc-extract-cms-2019-05.csv
data, feature_list = in_.get_input_features('../list_maker/data/20190705.csv', 200000, ID_=(23,"fuse")) #70000 #20191019.csv sc-extract-cms-2019-05.csv 95000 out.csv eos/gridftp eos/draining# 20190701_new.csv, 20190705.csv 23, 40000

i = 0
target_vals = in_.get_target(data)#, feature_list[i])
target_IDs = ["Throughput"]#feature_list[i]
t_feature_list = list(feature_list.copy())
t_feature_list = t_feature_list[2:] #for the most recent datasets: 20190701_new.csv, 20190705.csv
#t_feature_list.remove(t_feature_list[i])
target_vals = np.array(target_vals)
target_vals = target_vals.transpose()
target_vals = target_vals.reshape((target_vals.shape[0], 1,target_vals.shape[1]))
#print(data)
comb_features, bool_d = in_.organize_feature(data, target_vals, t_feature_list, mle)

out = out_.get_pred(data, target_vals, comb_features, mle, 10, bool_d) 
metrics, rr = out_.sel_pred(out, target_vals[int(round(len(target_vals)*0.8)):], i, bool_d)
#max_metrics = out_.sel_comb(metrics, 5) 
#ind_met = out_.get_ind(metrics, max_metrics) 
#print(ind_met)
"""
A = [0,0,0,0]#1/4,1/4,1/4,1/4]
mu = 0.25
c = 1/float(mu)
alpha = 0.25
print(metrics)
t = []
weights = [4, 4, 4, 4]
for i in range(len(metrics)):
    t.append(sum(metrics[i,0]))
loss = []
for i in range(len(A)):
    A[i] = weights[i]
    resultingoccurrence = out_.get_ind(metrics,A)#out_.recurrence(ind_met, len(comb_features))
    print(max(resultingoccurrence))
    print("index = " + str(resultingoccurrence.index(max(resultingoccurrence))))
    print(metrics[resultingoccurrence.index(max(resultingoccurrence))])
    loss.append(sum(metrics[resultingoccurrence.index(max(resultingoccurrence)),0]) - min(t))
    #print("loss = "+str(loss))
    A = [0,0,0,0]
nw = []
for i in range(len(loss)):
    nw.append(math.exp( -(mu*loss[i]) )*weights[i])
print(nw)
pool = 0
for i in range(len(loss)):
    pool += (1-(1-alpha)**loss[i])*nw[i]
new_weights = []
for i in range(len(loss)):
    new_weights.append(((1-alpha)**loss[i])*nw[i] + (1/(len(loss)-1))*(pool - (1-(1-alpha)**loss[i])*nw[i]))
print(new_weights)
"""
A = out_.set_weights(metrics, 15)#15
print(A)
resultingoccurrence = out_.get_ind(metrics,A)#, thnf=8)
print(resultingoccurrence)
selected_comb, dir_ = out_.sel_high_comb(resultingoccurrence, comb_features, rr)
print(metrics)
print(dir_)
print(metrics[len(selected_comb)-1])
print(len(selected_comb))
    

#start of tests

#prediction vs target 

mle.model = None
training_vals = in_.format_data(data, selected_comb, bool_d)
training_vals = np.array(training_vals)
training_vals = training_vals.transpose()
training_vals = training_vals.reshape((training_vals.shape[0], 1, training_vals.shape[1]))
mle.train(training_vals, target_vals, int(round(len(training_vals)*0.8)))
prediction = mle.predict(training_vals[int(round(len(training_vals)*0.8)):])

prediction = prediction.reshape((prediction.shape[0], prediction.shape[2]))
target_vals = target_vals.reshape((target_vals.shape[0], target_vals.shape[2]))


fig = plt.figure()
fig.set_figheight(4*len(target_IDs))
fig.set_figwidth(4)
iplot = 1
label = target_IDs#["Latency", "Throughput"]
for i in range(target_vals.shape[1]):#chosen_ports)):
    plt.subplot(len(label), 1,iplot)
    xs = np.arange(prediction.shape[0])
    t = target_vals[int(round(len(target_vals)*0.8)):, i]
    p = prediction[:, i]
    pp = []
    for j in range(len(t)):
        pp.append(p[j]/t[j])
    plt.plot(xs, t, lw=1)
    plt.plot(xs, p, lw=1)
    #plt.plot(xs, pp, lw=1)
    plt.title("Prediction vs Target " + label[i])
    plt.xlabel("timestep")
    plt.ylabel("Normalized "+label[i])
    if iplot==1:
        plt.legend(["target", "prediction"], loc="upper center", ncol=2, mode="expand", borderaxespad=0.)
    iplot += 1
plt.tight_layout()
fig.savefig("predicted_vs_target_L_T.png")

"""
#training/prediction experiment
f = open("timing.txt", "r")
content = f.read()
f.close
content = content.split("\n")
training = []
prediction = []

for i in range(len(content)-1):
    temp = content[i].split(",")
    training.append(float(temp[0]))
    prediction.append(float(temp[1]))

fig = plt.figure()
fig.set_figheight(16)
fig.set_figwidth(8)
iplot = 1
label = ["training", "prediction"]
for i in range(2):#chosen_ports)):
    plt.subplot(2, 1,iplot)
    xs = np.arange(len(training))
    t = training
    if i == 1:
        t = prediction
    plt.plot(xs, t, lw=1)
    plt.title("Time vs number of features\nused for " + label[i])
    plt.xlabel("Number of feature used")
    plt.ylabel("Latency of "+label[i]+" in s")
    #if iplot==1:
    #    plt.legend(["target", "prediction"], loc="upper center", ncol=2, mode="expand", borderaxespad=0.)
    iplot += 1
plt.tight_layout()
fig.savefig("time_analysis.png")
"""
