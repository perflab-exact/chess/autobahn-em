import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 18})
from get_input import input_data
from get_output import output_data
from MLE2 import MLE
import numpy as np
import statistics
import math
from scipy.spatial import distance
from sklearn.decomposition import PCA
import statistics
from scipy.stats import kurtosis
from scipy.stats import skew
from sklearn.manifold import TSNE
import umap
from sklearn.linear_model import Lasso, LogisticRegression
from sklearn.feature_selection import SelectFromModel
from sklearn.decomposition import SparsePCA
from sklearn.decomposition import PCA, IncrementalPCA
from keras import backend as K
from pystreamfs.algorithms import fsds, ofs, efs
from pystreamfs import pystreamfs
from keras.optimizers import RMSprop, Adam, SGD

def get_relative_error(pred, targ):
    # This function will calculate the relative error between target and predicted values
    RE = []
    for i in range(len(pred)):
        if targ[i] == 0:
            if pred[i] > targ[i]:
                RE.append(abs(pred[i]-targ[i]))
            else:
                RE.append(abs(targ[i]-pred[i]))
        else:
            if pred[i] > targ[i]:
                RE.append((abs(pred[i]-targ[i]))/targ[i])
            else:
                RE.append((abs(targ[i]-pred[i]))/targ[i])
    return RE

in_ = input_data()
out_ = output_data()
mle = MLE()
data, feature_list = in_.get_input_features('../../list_maker/data/20190705.csv', 350500, ID_=(23,"fuse"))#"eos/draining"))# 150000)#, ID_=(23,"eos/draining")) #sc-extract-cms-2019-05.csv 95000 out.csv ../list_maker/data/20191019.csv
i = 0
target_vals = in_.get_target(data)#, feature_list[i])
target_IDs = ["Throughput"]#feature_list[i]
t_feature_list = list(feature_list.copy())
target_vals = np.array(target_vals)
target_vals = target_vals.transpose()
target_vals = target_vals.reshape((target_vals.shape[0], 1,target_vals.shape[1]))
#temp = {}
#for w in data.keys():  
#    temp[w] = data[w][:95000]
#comb_features, bool_d = in_.organize_feature(temp, target_vals[:95000], t_feature_list, mle)

#print(target_vals.shape)
#print(data)
num_el = 35000#50000 # 95000
s_winnowML = []
acc_winnowML = []
s_PCA = []
s_PCAs = []
s_PCAi = []
s_FSDS = []
s_OFS = []
s_Lasso = []

for j in range(10):
    temp = {}
    for w in data.keys():
        temp[w] = data[w][j*num_el:(j+1)*num_el]
    #    #print(len(temp[w]))
    #mle = MLE()
    mle.model = None
    comb_features, bool_d = in_.organize_feature(temp, target_vals[j*num_el:(j+1)*num_el], t_feature_list, mle)
    out = out_.get_pred(temp, target_vals[j*num_el:(j+1)*num_el], comb_features, mle, 10, bool_d, round_=j)
    metrics, rr = out_.sel_pred(out, target_vals[int(round(len(target_vals[j*num_el:(j+1)*num_el])*0.8)):], i, bool_d)
    A = out_.set_weights(metrics, 100)#30)#15
    resultingoccurrence = out_.get_ind(metrics,A)
    selected_comb, dir_ = out_.sel_high_comb(resultingoccurrence, comb_features, rr)

    s_winnowML.append(len(selected_comb))
    acc_winnowML.append(metrics[len(selected_comb)-1])
    

    K.clear_session()
    print("New training")
    mle.model = None
    training_vals = in_.format_data(data, selected_comb, bool_d)
    training_vals = np.array(training_vals)
    training_vals = training_vals.transpose()
    if len(target_vals.shape) == 3:
        training_vals = training_vals.reshape((training_vals.shape[0], 1, training_vals.shape[1]))
    mle.train(training_vals, target_vals, int(round(len(training_vals)*0.8)),name=j)


    f = open("perf_data/Win_Fname.txt", "a")
    temp = ""
    for w in selected_comb:
        temp += w + ","
    temp = temp[:-1] + "\n"
    f.write(temp)
    f.close()


    print("Selected by WinnowML: " + str(s_winnowML))
    print(A)
    print(acc_winnowML)
    K.clear_session()

"""
for j in range(10):
    temp = {}
    for w in data.keys():
        temp[w] = data[w][j*num_el:(j+1)*num_el]

    metrics_PCA = []
    t = target_vals[j*num_el:(j+1)*num_el]
    mle.model = None
     
    for i in range(0,len(t_feature_list)):
        training_vals = in_.format_data(temp, t_feature_list, bool_d)
        training_vals = np.array(training_vals)
        training_vals = training_vals.transpose()

        pca = PCA(n_components=i+1)
        training_vals = pca.fit_transform(training_vals)
        print("length of the training vals "+ str(len(training_vals))+" and target vals " + str(len(t)))

        t = t[-len(training_vals):]
        print("length of the training vals "+ str(len(training_vals))+" and target vals " + str(len(t)))


        mle.model = None
        training_vals = training_vals.reshape((training_vals.shape[0], 1, training_vals.shape[1]))
        mle.train(training_vals, t, int(round(len(training_vals)*0.8)))
        prediction = mle.predict(training_vals[int(round(len(training_vals)*0.8)):])
        prediction = prediction.reshape((prediction.shape[0], prediction.shape[2]))
        t = t.reshape((t.shape[0], t.shape[2]))

        print(prediction.shape)
        print(t.shape)

        AE = get_relative_error(prediction[:,0], t[int(round(len(training_vals)*0.8)):,0])
        #print(AE)
        #kurt1 = kurtosis(target_vals[:, 0])
        #kurt2 = kurtosis(prediction[:, 0])
        #if kurt1 > kurt2:
        #    kurt = kurt1 - kurt2
        #else:
        #    kurt = kurt2 - kurt1
        #skew1 = skew(target_vals[:, 0])
        #skew2 = skew(prediction[:, 0])
        #if skew1 > skew2:
        #    skew3 = skew1 - skew2
        #else:
        #    skew3 = skew2 - skew1
        std = statistics.stdev(AE)
        metrics_PCA.append([sum(AE)/len(AE), std,0,0])#, kurt, skew3])
        t = t.reshape((t.shape[0], 1,t.shape[1]))

    print("========================================")

    metrics_SparsePCA = []
    for i in range(0,len(t_feature_list)):
        training_vals = in_.format_data(temp, t_feature_list, bool_d)
        training_vals = np.array(training_vals)
        training_vals = training_vals.transpose()
    
        sppca = SparsePCA(n_components=i+1)
        training_vals = sppca.fit_transform(training_vals)
        print(training_vals)

        mle.model = None
        training_vals = training_vals.reshape((training_vals.shape[0], 1, training_vals.shape[1]))
        mle.train(training_vals, t, int(round(len(training_vals)*0.8)))
        prediction = mle.predict(training_vals[int(round(len(training_vals)*0.8)):])
        prediction = prediction.reshape((prediction.shape[0], prediction.shape[2]))
        t = t.reshape((t.shape[0], t.shape[2]))

        print(prediction.shape)
        print(t.shape)

        AE = get_relative_error(prediction[:,0], t[int(round(len(training_vals)*0.8)):,0])
    
        #kurt1 = kurtosis(target_vals[:, 0])
        #kurt2 = kurtosis(prediction[:, 0])
        #if kurt1 > kurt2:
        #    kurt = kurt1 - kurt2
        #else:
        #    kurt = kurt2 - kurt1

        #skew1 = skew(target_vals[:, 0])
        #skew2 = skew(prediction[:, 0])
        #if skew1 > skew2:
        #    skew3 = skew1 - skew2
        #else:
        #    skew3 = skew2 - skew1

        std = statistics.stdev(AE)
        metrics_SparsePCA.append([sum(AE)/len(AE), std,0,0])#, kurt, skew3])
        t = t.reshape((t.shape[0], 1,t.shape[1]))


    print("========================================")

    metrics_IncrementalPCA = []
    for i in range(0,len(t_feature_list)):
        training_vals = in_.format_data(temp, t_feature_list, bool_d)
        training_vals = np.array(training_vals)
        training_vals = training_vals.transpose()
    
        ipca = IncrementalPCA(n_components=i+1)
        training_vals = ipca.fit_transform(training_vals)
        print(training_vals)

        mle.model = None
        training_vals = training_vals.reshape((training_vals.shape[0], 1, training_vals.shape[1]))
        mle.train(training_vals, t, int(round(len(training_vals)*0.8)))
        prediction = mle.predict(training_vals[int(round(len(training_vals)*0.8)):])
        prediction = prediction.reshape((prediction.shape[0], prediction.shape[2]))
        t = t.reshape((t.shape[0], t.shape[2]))
    
        print(prediction.shape)
        print(t.shape)

        AE = get_relative_error(prediction[:,0], t[int(round(len(training_vals)*0.8)):,0])
        std = statistics.stdev(AE)
        metrics_IncrementalPCA.append([sum(AE)/len(AE), std, 0, 0])#, kurt, skew3])
        t = t.reshape((t.shape[0], 1,t.shape[1]))
    
       
    print("========================================")

    metrics_FSDS = []
    FSDS_name = []
    bool_d = 1
    for i in range(0,len(t_feature_list)):
        training_vals = in_.format_data(temp, t_feature_list, bool_d)
        training_vals = np.array(training_vals)
        training_vals = training_vals.transpose()
        t = t.reshape((t.shape[0], t.shape[2]))
        t = t[-len(training_vals):]
        print(t.shape)
        print(training_vals.shape)

        step_size = training_vals.shape[len(training_vals.shape)-1] #number of input feature columns
        nb_features = t.shape[len(t.shape)-1] #number of target features

        model = mle.create_model2D(i+1, nb_features)
        rms=Adam() 
        model.compile(loss='mse', optimizer=rms, metrics=['mse'])
        model.summary()
        fs_algorithm = fsds.run_fsds
        #fs_algorithm = ofs.run_ofs
        param = dict()
        param['num_features'] = i+1  # number of features to return
        param['batch_size'] = 1000  # batch size

        param['B'] = []  # initial sketch matrix
        param['ell'] = 0  # initial sketch size
        param['k'] = 2#training_vals.shape[0]  # no. of singular values (can be equal to no. of clusters/classes -> here 2 for binary class.)
        param['m'] = training_vals.shape[1]  # no. of original features

        stats = pystreamfs.simulate_stream(training_vals, t , fs_algorithm, model, param)

        temp_ = stats['features']
        temp_arr = {}
        for q in range(len(temp_)):
            #temp_arr = {}
            #for q in range(len(temp)):
            str_ = ""
            for w in temp_[q]:
                str_ += str(w) + ","
            str_ = str_[:-1]
            if str_ in temp_arr.keys():
                temp_arr[str_] += 1
            else:
                temp_arr[str_] = 1
            a = sorted(temp_arr.items(), key=lambda x: x[1])
        num_feat = a[-1][0].split(",")
        num_feat_int = []
        for w in num_feat:
            num_feat_int.append(int(w))
        print(num_feat_int)

        feat = list(temp.keys())
        print(feat)

        selected_comb = []
        for q in num_feat_int:
            selected_comb.append(feat[q])

        print(selected_comb)

        FSDS_name.append(selected_comb)

        mle.model = None
        training_vals = in_.format_data(temp, selected_comb, 1)
        training_vals = np.array(training_vals)
        training_vals = training_vals.transpose()
        training_vals = training_vals.reshape((training_vals.shape[0], 1, training_vals.shape[1]))
        t = t.reshape((t.shape[0], 1,t.shape[1]))
        mle.train(training_vals, target_vals, int(round(len(training_vals)*0.8)))
        prediction_fsds = mle.predict(training_vals[int(round(len(training_vals)*0.8)):])
        prediction_fsds = prediction_fsds.reshape((prediction_fsds.shape[0], prediction_fsds.shape[2]))
        t = t.reshape((t.shape[0], t.shape[2]))

        print(t.shape)
        print(prediction_fsds.shape)

        AE = get_relative_error(prediction_fsds[:,0], t[int(round(len(training_vals)*0.8)):,0])
        std = statistics.stdev(AE)
        metrics_FSDS.append([sum(AE)/len(AE), std, 0, 0])#, kurt, skew3])                                                                                                                                    
        t = t.reshape((t.shape[0], 1,t.shape[1]))
    
    print("=======================================")

    metrics_OFS = []
    OFS_name = []
    bool_d = 1
    for i in range(0,len(t_feature_list)):
        training_vals = in_.format_data(temp, t_feature_list, bool_d)
        training_vals = np.array(training_vals)
        training_vals = training_vals.transpose()
        t = t.reshape((t.shape[0], t.shape[2]))
        t = t[-len(training_vals):]
        print(t.shape)
        print(training_vals.shape)

        step_size = training_vals.shape[len(training_vals.shape)-1] #number of input feature columns
        nb_features = t.shape[len(t.shape)-1] #number of target features
        
        model = mle.create_model2D(i+1, nb_features)
        rms=Adam()
        model.compile(loss='mse', optimizer=rms, metrics=['mse'])
        model.summary()
        #fs_algorithm = fsds.run_fsds
        fs_algorithm = ofs.run_ofs
        param = dict()
        param['num_features'] = i+1  # number of features to return 
        param['batch_size'] = 1000  # batch size  
            
        #param['B'] = []  # initial sketch matrix 
        #param['ell'] = 0  # initial sketch size 
        #param['k'] = 2#training_vals.shape[0]  # no. of singular values (can be equal to no. of clusters/classes -> here 2 for binary class.)
        #param['m'] = training_vals.shape[1]  # no. of original features 
        
        stats = pystreamfs.simulate_stream(training_vals, t , fs_algorithm, model, param)
        temp_ = stats['features']
        temp_arr = {}
        for q in range(len(temp_)):
            #temp_arr = {}
            #for q in range(len(temp)):
            str_ = ""
            for w in temp_[q]:
                str_ += str(w) + ","
            str_ = str_[:-1]
            if str_ in temp_arr.keys():
                temp_arr[str_] += 1
            else:
                temp_arr[str_] = 1
            a = sorted(temp_arr.items(), key=lambda x: x[1])
        num_feat = a[-1][0].split(",")
        num_feat_int = []
        for w in num_feat:
            num_feat_int.append(int(w))
        print(num_feat_int)
        feat = list(temp.keys())
        print(feat)

        selected_comb = []
        for q in num_feat_int:
            selected_comb.append(feat[q])

        print(selected_comb)
        
        OFS_name.append(selected_comb)

        mle.model = None
        training_vals = in_.format_data(temp, selected_comb, 1)
        training_vals = np.array(training_vals)
        training_vals = training_vals.transpose()
        training_vals = training_vals.reshape((training_vals.shape[0], 1, training_vals.shape[1]))
        t = t.reshape((t.shape[0], 1,t.shape[1]))
        mle.train(training_vals, target_vals, int(round(len(training_vals)*0.8)))
        prediction_fsds = mle.predict(training_vals[int(round(len(training_vals)*0.8)):])
        prediction_fsds = prediction_fsds.reshape((prediction_fsds.shape[0], prediction_fsds.shape[2]))
        t = t.reshape((t.shape[0], t.shape[2]))
        
        print(t.shape)
        print(prediction_fsds.shape) 
         
        AE = get_relative_error(prediction_fsds[:,0], t[int(round(len(training_vals)*0.8)):,0])   
        std = statistics.stdev(AE)
        metrics_OFS.append([sum(AE)/len(AE), std, 0, 0])#, kurt, skew3])        
        t = t.reshape((t.shape[0], 1,t.shape[1])) 
          
    print("=======================================")

    metrics_ = []
    for i in range(len(metrics)):
        metrics_.append(metrics[i].transpose())
    metrics = np.array(metrics_)
    metrics = metrics.reshape((metrics.shape[0], metrics.shape[1]))
    print(metrics.shape)
    print("******************************")
    metrics_PCA = np.array(metrics_PCA)
    print(metrics_PCA.shape)
    print("******************************")
    metrics_SparsePCA = np.array(metrics_SparsePCA)
    print(metrics_SparsePCA.shape)
    print("******************************")
    metrics_IncrementalPCA = np.array(metrics_IncrementalPCA)
    print(metrics_IncrementalPCA.shape)

    #print("******************************")
    #metrics_FSDS = np.array(metrics_FSDS)
    #print(metrics_FSDS.shape)
    #print("******************************")
    #metrics_OFS = np.array(metrics_OFS)
    #print(metrics_OFS.shape)


    #fig = plt.figure()
    #fig.set_figheight(12)
    #fig.set_figwidth(12)

    ##########PCA#####################

    metrics_ = []
    for i in range(len(metrics_PCA)):
        tt = metrics_PCA[i].reshape((len(metrics_PCA[i]), 1))
        metrics_.append(tt.transpose())
    metrics_PCA = np.array(metrics_)


    x = range(1,len(metrics)+1)
    A = out_.set_weights(metrics_PCA, 100)#15
    ID_PCA = out_.get_ind(metrics_PCA,A)
    max_ = max(ID_PCA)
    ind_PCA = ID_PCA.index(max_) + 1
    s_PCA.append(ind_PCA)
    print(s_PCA)

    metrics_ = []
    for i in range(len(metrics_PCA)):
        metrics_.append(metrics_PCA[i].transpose())
    metrics_PCA = np.array(metrics_)
    metrics_PCA = metrics_PCA.reshape((metrics_PCA.shape[0], metrics_PCA.shape[1]))

    f = open("perf_data/PCA_NF.txt", "a")
    f.write(str(ind_PCA) + "\n")
    f.close()
    f = open("perf_data/PCA_acc.txt", "a")
    temp_ = ""
    for w in metrics_PCA[ind_PCA-1]:
        temp_ += str(w)+","
    temp_ = temp_[:-1]+"\n"
    f.write(temp_)
    f.close()

    ##########SparsePCA#####################                                                     

    metrics_ = [] 
    for i in range(len(metrics_PCA)):  
        tt = metrics_SparsePCA[i].reshape((len(metrics_SparsePCA[i]), 1))
        metrics_.append(tt.transpose()) 
    metrics_SparsePCA = np.array(metrics_)        


    x = range(1,len(metrics)+1)    
    A = out_.set_weights(metrics_SparsePCA, 100)#15
    ID_SparsePCA = out_.get_ind(metrics_SparsePCA,A)
    max_ = max(ID_SparsePCA)
    ind_SparsePCA = ID_SparsePCA.index(max_) + 1
    s_PCAs.append(ind_SparsePCA)
    print("Sparse PCA")
    print(s_PCAs)

    metrics_ = []
    for i in range(len(metrics_SparsePCA)):
        metrics_.append(metrics_SparsePCA[i].transpose())
    metrics_SparsePCA = np.array(metrics_)
    metrics_SparsePCA = metrics_SparsePCA.reshape((metrics_SparsePCA.shape[0], metrics_SparsePCA.shape[1]))

    f = open("perf_data/spPCA_NF.txt", "a")
    f.write(str(ind_SparsePCA) + "\n")
    f.close()
    f = open("perf_data/spPCA_acc.txt", "a")
    temp_ = ""
    for w in metrics_SparsePCA[ind_SparsePCA-1]:
        temp_ += str(w)+","
    temp_ = temp_[:-1]+"\n"
    f.write(temp_)
    f.close()

    ############IncrementalPCA#########

    metrics_ = []
    for i in range(len(metrics_IncrementalPCA)):
        tt = metrics_IncrementalPCA[i].reshape((len(metrics_IncrementalPCA[i]), 1))
        metrics_.append(tt.transpose())
    metrics_IncrementalPCA = np.array(metrics_)

    x = range(1,len(metrics)+1)
    A = out_.set_weights(metrics_IncrementalPCA, 100)#15
    ID_IncrementalPCA = out_.get_ind(metrics_IncrementalPCA,A)
    max_ = max(ID_IncrementalPCA)
    ind_IncrementalPCA = ID_IncrementalPCA.index(max_) + 1
    s_PCAi.append(ind_IncrementalPCA)
    print("Incremental PCA")
    print(s_PCAi)

    metrics_ = []
    for i in range(len(metrics_IncrementalPCA)):
        metrics_.append(metrics_IncrementalPCA[i].transpose())
    metrics_IncrementalPCA = np.array(metrics_)
    metrics_IncrementalPCA = metrics_IncrementalPCA.reshape((metrics_IncrementalPCA.shape[0], metrics_IncrementalPCA.shape[1]))
    
    f = open("perf_data/incPCA_NF.txt", "a")
    f.write(str(ind_IncrementalPCA) + "\n")
    f.close()
    f = open("perf_data/incPCA_acc.txt", "a")
    temp_ = ""
    for w in metrics_IncrementalPCA[ind_IncrementalPCA-1]:
        temp_ += str(w)+","
    temp_ = temp_[:-1]+"\n"
    f.write(temp_)
    f.close()

    
    ###############FSDS#####################
    
    metrics_ = []
    for i in range(len(metrics_FSDS)):
        tt = metrics_FSDS[i].reshape((len(metrics_FSDS[i]), 1))
        metrics_.append(tt.transpose())
    metrics_FSDS = np.array(metrics_)

    x = range(1,len(metrics)+1)
    A = out_.set_weights(metrics_FSDS, 100)
    ID_FSDS = out_.get_ind(metrics_FSDS,A)
    max_ = max(ID_FSDS)
    ind_FSDS = ID_FSDS.index(max_) + 1
    s_FSDS.append(ind_FSDS)
    print("FSDS")
    print(s_FSDS)

    metrics_ = []
    for i in range(len(metrics_FSDS)):
        metrics_.append(metrics_FSDS[i].transpose())
    metrics_FSDS = np.array(metrics_)
    metrics_FSDS = metrics_FSDS.reshape((metrics_FSDS.shape[0], metrics_FSDS.shape[1]))

    f = open("perf_data/FSDS_NF.txt", "a")
    f.write(str(ind_FSDS) + "\n")
    f.close()

    f = open("perf_data/FSDS_acc.txt", "a")
    temp_ = ""
    for w in metrics_FSDS[ind_FSDS-1]:
        temp_ += str(w)+","
    temp_ = temp_[:-1]+"\n"
    f.write(temp_)
    f.close()


    f = open("perf_data/FSDS_Fname.txt", "a")
    temp_ = ""
    for w in FSDS_name[ind_FSDS-1]:
         temp_ += w + ","
    temp_ = temp_[:-1] + "\n"
    f.write(temp_)
    f.close()
    
    
    ###############OFS#####################
    metrics_ = []
    for i in range(len(metrics_OFS)):
        tt = metrics_OFS[i].reshape((len(metrics_OFS[i]), 1))
        metrics_.append(tt.transpose())
    metrics_OFS = np.array(metrics_)
    
    x = range(1,len(metrics)+1)
    A = out_.set_weights(metrics_OFS, 100)
    ID_OFS = out_.get_ind(metrics_OFS,A)
    max_ = max(ID_OFS)
    ind_OFS = ID_OFS.index(max_) + 1
    s_OFS.append(ind_OFS)
    print("OFS")
    print(s_OFS)

    metrics_ = []
    for i in range(len(metrics_OFS)):
        metrics_.append(metrics_OFS[i].transpose())
    metrics_OFS = np.array(metrics_)
    metrics_OFS = metrics_OFS.reshape((metrics_OFS.shape[0], metrics_OFS.shape[1]))
    
    f = open("perf_data/OFS_NF.txt", "a")
    f.write(str(ind_OFS) + "\n")
    f.close()
    
    f = open("perf_data/OFS_acc.txt", "a")
    temp_ = ""
    for w in metrics_OFS[ind_OFS-1]:
        temp_ += str(w)+","
    temp_ = temp_[:-1]+"\n"
    f.write(temp_)
    f.close()

    f = open("perf_data/OFS_Fname.txt", "a")
    temp_ = ""
    for w in OFS_name[ind_OFS-1]:
        temp_ += w + ","
    temp_ = temp_[:-1] + "\n"
    f.write(temp_)
    f.close()

    
    ##########l1-regulirized#####################

    al = [0.01, 0.001, 0.0001]
    count = []
    metrics_l1 = []
    t_ind = 0
    for i in al:
        print(i)
        training_vals = in_.format_data(temp, t_feature_list, bool_d)
        training_vals = np.array(training_vals)
        training_vals = training_vals.transpose()
        training_vals = training_vals.reshape((training_vals.shape[0],  training_vals.shape[1]))
        if len(t.shape) == 3:
            t = t.reshape((t.shape[0], t.shape[2]))
        if len(training_vals.shape) == 3:
            training_vals = training_vals.reshape((training_vals.shape[0], training_vals.shape[2]))
        lasso = Lasso(alpha=i, max_iter=10e5)
        lasso.fit(training_vals, t[-len(training_vals):])
        print(lasso.coef_)
        count.append(np.sum(lasso.coef_!=0))
        selected_comb_l1 = []
        for n in range(len(lasso.coef_)):
            if lasso.coef_[n]!=0:
                selected_comb_l1.append(feature_list[n])
        mle.model = None    
        training_vals = in_.format_data(temp, selected_comb_l1, bool_d)
        training_vals = np.array(training_vals)
        training_vals = training_vals.transpose()
        training_vals = training_vals.reshape((training_vals.shape[0], 1, training_vals.shape[1]))
        t = t.reshape((t.shape[0], 1,t.shape[1]))
        mle.train(training_vals, t, int(round(len(training_vals)*0.8)))                                                                                                                                          
        prediction_l1 = mle.predict(training_vals[int(round(len(training_vals)*0.8)):])
        t = t.reshape((t.shape[0], t.shape[2]))
    
        print(prediction.shape)
        print(t.shape)

        AE = get_relative_error(prediction[:,0], t[int(round(len(training_vals)*0.8)):,0])
        std = statistics.stdev(AE)
        metrics_l1.append([sum(AE)/len(AE), std, 0,0])#kurt, skew3])
        f = open("perf_data/lasso"+str(t_ind)+"_acc.txt", "a")
        f.write(str(sum(AE)/len(AE))+","+str(std) + "0,0\n")
        f.close()
        f = open("perf_data/lasso"+str(t_ind)+"_NF.txt", "a")
        f.write(str(len(selected_comb_l1))+"\n")
        f.close()
        f = open("perf_data/lasso"+str(t_ind)+"_Fname.txt", "a")
        temp_ = ""
        for w in selected_comb_l1:
            temp_ += w + ","
        temp_ = temp_[:-1] + "\n"
        f.write(temp_)
        f.close()
        t_ind += 1
    s_Lasso.append(count)
    print("Selected by Lasso")
    print(s_Lasso)
    K.clear_session()
"""
print("Selected by WinnowML: " + str(s_winnowML))
f = open("perf_data/Win_NF.txt", "a")
for i in s_winnowML:
    f.write(str(i) + "\n")
f.close()
f = open("perf_data/Win_acc.txt", "a")
print(acc_winnowML)
for i in acc_winnowML:
    temp = ""
    for j in i[0]:
        temp += str(j) + ","
    temp = temp[:-1] + "\n"
    f.write(temp)
f.close()


