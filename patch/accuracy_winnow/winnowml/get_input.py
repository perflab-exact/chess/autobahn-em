#this file will contain the code for the combination of the input features culumns
import csv
import numpy as np
np.random.seed(3900)
import itertools
import random
import math
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2, f_regression
from sklearn.feature_selection import RFE
from sklearn.linear_model import LogisticRegression
from sklearn.decomposition import PCA
from sklearn.ensemble import ExtraTreesClassifier
#from tensorflow.keras.optimizers import RMSprop, Adam, SGD
import eli5
from eli5.sklearn import PermutationImportance
from collections import Counter
from imblearn.over_sampling import SMOTE
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from scipy.stats import pearsonr, spearmanr
from scipy.stats import kendalltau
from scipy import signal

class input_data:
    dfscores = {}
    def areElementsContiguous(self, arr, n): 
        # Sort the array 
        arr.sort() 
        # After sorting, check if  
        # current element is either 
        # same as previous or is  
        # one more. 
        for i in range(1,n): 
            if (arr[i] - arr[i-1] == 1) : 
                return 0
        return 1
    def is_number(self,s):
        try:
            float(s)
            return True
        except ValueError:
            return False

    def moving_average(self,a, n=3) :
        b = []
        for i in range(len(a)-1):
            b.append(float(a[i]))
        ret = np.cumsum(b, dtype=float)
        ret[n:] = ret[n:] - ret[:-n]
        return ret[n - 1:] / n



    def get_input_features(self, name, num, start=0, invert=False, balance=-1, ID_ = None):
        #gets the input features from the input access log
        data = []
        count = 0
        with open(name) as csvfile:
            readCSV = csv.reader(csvfile, delimiter=',')
            for row in readCSV:
                #for i in range(len(row)):
                #    if row[i] = "N/A":
                #        row[i] = 0
                if "deletion" not in row: 
                    #print(row)
                    if len(row) == 1:
                        row = row[0].split('\t')
                    elif row[-1] == "":
                        row = row[:-1]
                    #for i in range(len(row)):
                    #    if self.is_number(row[i]):
                    #        x = float(row[i])
                    #        if math.isnan(x):
                    #            row[i] = 0
                    if len(data)>0 and len(row)==len(data[0]) or len(data)==0:
                        data.append(row)
                    if count > 2500000:#10000000:#2500000:#0:
                        break
                    count += 1
        get_workload_id = []
        if ID_ != None:
            n_data = [data[0]]
            for i in range(1,len(data)):
                if self.is_number(ID_[1]):
                    t = float(ID_[1])
                    t2 = float(data[i][ID_[0]])
                else:
                    t = ID_[1]
                    t2 = data[i][ID_[0]]
                if t2 == t:
                    temp = []
                    for j in data[i]:
                        temp.append(j)
                    n_data.append(temp)
                if t2 not in get_workload_id:
                    get_workload_id.append(t2)
            #data_t = data[0]
            print(get_workload_id)
            data =  n_data
            data = np.array(data)

        #limiting the amount of rows because of memory issue
        if balance >= 0:
            dead = []
            alive = []
            for i in range(1,len(data)):
                if isinstance(data[i][balance], str) and (data[i][balance] == 'pos' or  data[i][balance] == 'neg'):
                    if data[i][balance] == 'pos':
                        dead.append(data[i])
                    elif data[i][balance] == 'neg':
                        alive.append(data[i])
                else:
                    if float(data[i][balance]) == 1.0:
                        dead.append(data[i])
                    elif float(data[i][balance]) == 0.0:
                        alive.append(data[i])
        
            data_ = dead
            for ii in range(num-len(dead)):
                data_.append(alive[ii])
            np.random.shuffle(data_)
            #data_ = data_[::-1]
            data_[0] = data[0]
            data = np.array(data_) 
        else:
            if invert:
                data[-1] = data[0]
                data = np.array(data[-num:])
                data = data[::-1]
            else:
                data = np.array(data[:num])
                data_ = data[1:]
                np.random.shuffle(data_)
                data_[0] = data[0]
                data = np.array(data_)
                #print(data)


        if data[0,0] == "":
            feature_values = data[1:, 1:]
            feature_ID = data[0, 1:]
        else:
            feature_values = data[1:]
            feature_ID = data[0]
        data_dict = {}
        found_directories = {0:[], 1:[], 2:[]}
        for i in range(len(feature_ID)):
            for j in range(len(feature_values[:,i])):
                if self.is_number(feature_values[j,i]):
                    feature_values[j,i] = float(feature_values[j,i])
                elif len(feature_values[j,i]) == 0:
                    feature_values[j,i] = 0
                else:
                    split_ = feature_values[j,i].split("/")
                    index = 0
                    if len(split_) > 3:
                        split_ = split_[1:]
                        for x in range(3):
                            if split_[x] not in found_directories[x]:
                                found_directories[x].append(split_[x])
                                index += len(found_directories[x])
                            else:
                                index += found_directories[x].index(split_[x])

                    else:
                        for x in range(len(split_)):
                            if split_[x] not in found_directories[x]:
                                found_directories[x].append(split_[x])
                                index += len(found_directories[x])
                            else:
                                index += found_directories[x].index(split_[x])
                    feature_values[j,i] = index#float(index)

            
            data_dict[feature_ID[i]] = np.array(feature_values[:,i])
        return (data_dict, feature_ID)


    def get_combination(self,X, num):
        # creates the possible combinations of the input features
        # X is the list of feature ID
        stuff = [1, num, len(X)]
        out = []
        #limited to 5 long since I kept running out of memory
        for L in stuff:
            for subset in itertools.combinations(X, L):
                out.append(subset)
        return out

    def organize_feature(self, data, target, features, mle, keras=False, sklearn=False, smooth=None, corr_weight=1):
        #This function will organize the feature by importance using four approaches 
        Y = target
        if len(target.shape) > 2:
            Y = target.reshape((target.shape[0], target.shape[2]))
        target_tmp = list(Y[:,0])
        bool_d = self.areElementsContiguous(target_tmp[:], len(target_tmp))
        X = []
        for i in features:
            data_1 = np.array(data[i])
            #print(np.any(np.isfinite(data_1)))
            #print(np.any(np.isnan(data_1)))
            print(data_1)
            data_1 = data_1.astype('float')
            #if i == "fid":
            #    for j in range(len(data_1)):
            #        data_1[j] = data_1[j]-min(data_1)
            #        if np.isnan(data_1[j]):
            #            data_1[j] = 0
            if bool_d == 0:
                data_ = data_1
            else:
                if smooth==None:
                    data_ = data_1
                else:
                    data_ = self.moving_average(data_1, smooth)
            data_ = data_.reshape((data_.shape[0], 1))
            scaler = StandardScaler()
            data_ = scaler.fit_transform(data_)
            X.append(data_.reshape((data_.shape[0],)))
        X = np.array(X)
        X = X.transpose()
        out_ = [0]*X.shape[1]
        for i in range(Y.shape[1]):
            #Format the target value that is used for importance
            target_ = list(Y[:,i])
            #print(target_)
            bool_d = self.areElementsContiguous(target_[:], len(target_))
            target_ = np.array(target_)
            target_ = target_.reshape((target_.shape[0], 1))
            target_ = target_.astype('int')
            training_test_split = int(round(len(X)*0.8))
            if len(X) < len(target_):
                target_ = target_[-len(X):]
            print(X.shape)
            print("44444444444444444444444444444444444444444444444444")
            model = mle.train(X, target_, training_test_split)#mle.create_model2D(X.shape[1], target_.shape[1])#ExtraTreesClassifier()
            if bool_d == 0:
                if keras:
                    w = np.array(model.get_weights()[0])
                    we = []
                    for i in w:
                        we.append(sum(i))
                    score2 = np.array(we)
                elif sklearn:
                    w = model.coef_
                    print(model.classes_)
                    print(w.shape)
                    we = w[0]#[]
                    score2 = np.array(we)
                else:
                    perm = PermutationImportance(model,scoring="f1", random_state=1).fit(X[training_test_split:],target_[training_test_split:])
                    score2 = perm.feature_importances_
            else:
                #r2 neg_mean_squared_error
                print("Getting the weights")
                bestfeatures = SelectKBest(score_func=chi2, k=10)
                x_ = MinMaxScaler().fit_transform(X)
                #x_ = self.moving_average(x_[0], 20)
                t_ = MinMaxScaler().fit_transform(target_)
                #t_ = self.moving_average(t_, 20)
                fit = bestfeatures.fit(x_,t_)
                dfscores_ = fit.scores_
                dfscores1 = []
                count1 = 0
                w1 = np.array(model.get_weights()[0])
                we = model.get_weights()
                gro = round(len(model.get_weights()[0][0])/X.shape[1])
                if True:#len(w1) == 1:
                    w = []
                    for m in range(len(we)-1):
                        ww = np.array(we[m])
                        if len(ww.shape) == 1 and ww.shape[0] >= X.shape[1]:
                            ww = ww.reshape(len(ww), 1)
                            ww = ww.transpose()
                        if len(ww.shape) == 2 and ww.shape[1] < X.shape[1] and ww.shape[0] >= X.shape[1]:
                            ww = ww.transpose()
                        for p in range(ww.shape[0]):
                            if ww[p].shape[0] < X.shape[1]:
                                ww_p = ww[p].transpose()
                            else:
                                ww_p = ww[p]
                            gro = round(len(ww_p)/X.shape[1])
                            c = 0
                            w_temp = []
                            for n in range(0, len(ww_p)-gro+1, gro):
                                temp_weight = sum(ww_p[n:n+gro])/len(ww_p[n:n+gro])
                                w_temp.append(temp_weight)
                            if len(w_temp) != X.shape[1]:
                                print(len(w_temp))
                                w_temp = w_temp[:X.shape[1]]
                                #w.append(w_temp)
                            elif len(w_temp) == X.shape[1]:
                                w.append(w_temp)
                    w = np.array(w)
                    print(w.shape)
                    w = w.transpose()
                    print(w.shape)
                    print("##########################################################")
                else:
                    w = w1
                #print(w)
                #print(len(w))
                #print(round(len(model.get_weights()[0][0])/X.shape[1]))
                #print(len(model.get_weights()[-2]))
                for d in dfscores_:
                    #corr_ = signal.correlate(x_[:, count1],  t_[:, 0])#, mode='same')
                    #corr = sum(corr_)/len(corr_)
                    #corr, _ = spearmanr( target_[:, 0], X[:, count1])
                    #corr, p = kendalltau(x_[:, count1],  t_[:, 0])
                    corr = np.corrcoef(X[:, count1], target_[:,0])
                    
                    
                    #corr = np.cov(X[:, count1], target_[:,0])
                    #print(corr)
                    corr = corr[0,1]
                    if math.isnan(corr):
                        corr = 0
                    if corr < 0:
                        corr = -corr
                    #print(str(corr) + " vs "+ str(sum(w[count1])) )#+ " p = " + str(p))
                    sum_weight = sum(w[count1])
                    #if sum_weight < 0:
                    #    sum_weight = -sum_weight
                    d = sum_weight+ corr_weight*corr #2100
                    if math.isnan(d):
                        d = 0
                    #if d < 0 :
                    #    d = -d
                    #d += w[count1][0]
                    if not features[count1] in self.dfscores.keys():
                        self.dfscores[features[count1]] = [d]
                    else:
                        self.dfscores[features[count1]].append(d)
                    dfscores1.append(sum(self.dfscores[features[count1]])/len(self.dfscores[features[count1]]))
                    count1 += 1
                """
                w = np.array(model.get_weights()[0])
                we = []
                
                count = 0
                for d in w:
                    corr, _ = spearmanr(X[:, count],  target_[:, 0])
                    if math.isnan(corr):
                        corr = 0
                    if corr >= 0:
                        we.append(corr)#i[0]+corr)
                    else:
                        we.append(-corr)#-i[0]+corr)
                    count += 1

                we = np.array(we)
                """
                #perm = PermutationImportance(model,scoring="neg_mean_squared_error", random_state=1).fit(X[training_test_split:],target_[training_test_split:])
                score2 = dfscores1#we#perm.feature_importances_ #we
            print("===============================")
            print(score2)
            print("==============================")
            for j in range(len(score2)):
                if bool_d == 0 and score2[j] < 0:
                    out_[j] -= score2[j]
                else:
                    out_[j] += score2[j]
            print("==========================================")

        temp = sorted(out_)
        temp = temp[::-1]
        organized_features = []
        print("CHECK HERE")
        print(features)
        print(out_)
        for i in temp:
            organized_features.append(features[out_.index(i)])
            out_[out_.index(i)] = "N/A"
        #print(organized_features) 
        #organized_features = organized_features[::-1]
        print(organized_features)

        groups_of_features = []
        for i in range(1,len(organized_features)):
            groups_of_features.append(organized_features[:i])
        groups_of_features.append(organized_features)
        print(groups_of_features)
        return (groups_of_features, bool_d)

    def get_target(self,X,index=0, time_open_columns=None, time_close_columns=None, bytes_used_columns=None, smooth=None):
        # create the dataset of target values 
        # X is the whole training feature dataset
        #Currently I will focuse on modeling latency and throughut of incomming workloads
        target_vals = []

        if index==0:
            open_ = []
            for i in range(len(X[time_open_columns[0]])):
                ots = 0
                for y in range(len(time_open_columns)):
                    #if not (time_open_columns[y] == None): 
                    ots += float(X[time_open_columns[y]][i])/10**(y*3)
                open_.append(ots)

            close_ = []
            for i in range(len(X[time_close_columns[0]])):  
                cts = 0
                for y in range(len(time_close_columns)):
                    #if not (time_close_columns[y] == None):                                                                                                                                                                  
                    cts += float(X[time_close_columns[y]][i])/10**(y*3)
                close_.append(cts)

            bytes_ = []
            for i in range(len(X[bytes_used_columns[0]])):
                b = 0
                for y in range(len(bytes_used_columns)):
                    #if not (bytes_used_columns[y] == None):
                    b += float(X[bytes_used_columns[y]][i])
                bytes_.append(b)

        print(close_[0])
        print("--------------------------------------------------")
        print()
        print(open_[0])
        print("--------------------------------------------------")
        print()
        print(bytes_[0])
        print("--------------------------------------------------")
        print()

        if index==0:
            latency_ = []
            TP_ = []
            for i in range(len(X[time_open_columns[0]])):
                latency_.append(abs(close_[i]-open_[i]))
                #latency_.append(-((float(X['cts'][i])+(float(X['ctms'][i])/1000)) - (float(X['ots'][i])+(float(X['otms'][i])/1000)) ))
                if latency_[i] == 0:
                    TP_.append(0.0)
                else:
                    TP_.append(bytes_[i]/latency_[i])
                    #TP_.append((float(X['wb'][i])+float(X['rb'][i]))/latency_[i])
        
            data_1 = np.array(TP_)
            if smooth==None:
                data_ = data_1
            else:
                data_ = self.moving_average(data_1, smooth)
            data_ = data_.reshape((data_.shape[0], 1))
            scaler = MinMaxScaler(feature_range=(0, 1))
            data_ = scaler.fit_transform(data_)
            target_vals.append(data_.reshape((data_.shape[0],)))
        elif isinstance(index, np.ndarray):
            for i in index:
                data_1 = np.array(X[i])
                if smooth==None: 
                    data_ = data_1
                else:
                    data_ = self.moving_average(data_1, smooth)
                data_ = data_.reshape((data_.shape[0], 1))
                scaler = MinMaxScaler(feature_range=(1, 2))
                data_ = scaler.fit_transform(data_)
                target_vals.append(data_.reshape((data_.shape[0],)))
                result = X.pop(i, None)
        else:
            data_1 = np.array(X[index])
            if smooth==None:
                data_ = data_1#[:-499]
            else:
                data_ = self.moving_average(data_1, smooth)
            strings = {}
            count = 0
            for i in range(len(data_)):
                if isinstance(data_[i], str) and data_[i] not in strings.keys():
                    strings[data_[i]] = count
                    count += 1
                if isinstance(data_[i], str):
                    data_[i] = strings[data_[i]]

            print(strings)
            data_ = data_.reshape((data_.shape[0], 1))
            data_ = data_.astype('float')

            target_vals.append(data_.reshape((data_.shape[0],)))
            result = X.pop(index, None)


        return target_vals

    def format_data(self, X, features, bool_d, smooth=None):
        #will format the data as training features and target values
        training_vals = []
        for i in features:
            data_1 = np.array(X[i])
            data_1 = data_1.astype('float')
            if bool_d == 0:
                data_ = data_1
            else:
                if smooth==None:
                    data_ = data_1
                else:
                    data_ = self.moving_average(data_1, smooth)
            data_ = data_.reshape((data_.shape[0], 1))
            if bool_d == 0:
                scaler = StandardScaler()#MinMaxScaler(feature_range=(0,1))
            else:
                scaler = StandardScaler()#MinMaxScaler(feature_range=(0,3))#1, 2))
            data_ = scaler.fit_transform(data_)
            training_vals.append(data_.reshape((data_.shape[0],)))
            #training_vals.append(X[i])
        return training_vals

