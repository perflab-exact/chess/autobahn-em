#This file contains the neural network that will map the training features to the target values
import os
import numpy as np
np.random.seed(3900) #5400
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="0"
import keras
import time
import math
from keras.constraints import maxnorm
from keras.models import Model, load_model
from keras.models import Sequential
from keras.layers import Input, GaussianNoise, Embedding, BatchNormalization
from keras.layers import Dense, Dropout, Activation, Flatten, TimeDistributed
from keras.layers import Conv1D, MaxPooling1D, LeakyReLU, PReLU
from keras.utils import np_utils
from keras.callbacks import CSVLogger, ModelCheckpoint
from keras.regularizers import l2
import h5py
from keras.layers import LSTM, CuDNNGRU, Reshape, CuDNNLSTM, Bidirectional
import tensorflow as tf
from tensorflow.python.client import device_lib
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from keras.backend.tensorflow_backend import set_session
#from keras import backend as K
import keras.backend as K
from math import sqrt
from sklearn.linear_model import LogisticRegression
from keras.optimizers import RMSprop, Adam, SGD
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn import svm
from sklearn.neighbors import KNeighborsClassifier
from sklearn.feature_extraction.text import TfidfVectorizer
from imblearn.under_sampling import RandomUnderSampler
from imblearn.over_sampling import RandomOverSampler, SMOTE
from imblearn.pipeline import make_pipeline as make_pipeline_imb
from sklearn.pipeline import Pipeline
from keras.layers import Layer
from keras.wrappers.scikit_learn import KerasClassifier
class Round(Layer):
    def __init__(self, **kwargs):
        super(Round, self).__init__(**kwargs)

    def get_output(self, train=False):
        X = self.get_input(train)
        return round(X)

    def get_config(self):
        config = {"name": self.__class__.__name__}
        base_config = super(Round, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))
                                                                    

class MLE:
    model = None
    def printDistinct(self, arr, n): 
        # Pick all elements one by one 
        for i in range(0, n): 
            # Check if the picked element  
            # is already printed 
            d = 0
            for j in range(0, i): 
                if (arr[i] == arr[j]): 
                    d = 1
                    break
                                                                                                      
            # If not printed earlier, 
            # then print it 
            if (d == 0): 
                print(arr[i])

    def create_model2D(self, step_size, nb_features):
        inputs = Input(shape=(step_size,))
        #if step_size > 1:
        #    step_size = step_size/2
        #preds = Dense(int(round(step_size)), activation='linear')(inputs)
        #preds = Dense(int(round(step_size)), activation='linear')(preds)
        #preds = Dense(64, activation='linear')(preds)
        #preds = Dense(4, activation='elu')(preds)
        #preds = Dense(2, activation='elu')(preds)
        #preds = Reshape((1, step_size))(inputs)
        #preds = LSTM(step_size, return_sequences=True)(preds)
        #preds = Reshape((step_size, ))(preds)
        #preds = Dense(step_size, activation='linear')(inputs)
        #preds = Dense(step_size, activation='relu')(preds)
        #preds = Dense(nb_features, activation='sigmoid')(preds)
        #preds = Dense(nb_features, activation='sigmoid')(inputs)
        #preds = Round()(preds)
        #model = Model(inputs=inputs,outputs=preds)
        #rms=Adam()#Adam()#RMSprop(lr=0.001) #Standard gradient descent (keep things simple)
        #model.compile(loss='binary_crossentropy', optimizer=rms, metrics=['accuracy'])#(loss='binary_crossentropy', optimizer=rms, metrics=['binary_accuracy'])
        #pipeline = model#Pipeline([('standardize',round()), ('mlp',model)])
        pipeline = LogisticRegression()#DecisionTreeClassifier(random_state=104, max_depth=1000)#LogisticRegression()#make_pipeline_imb(RandomOverSampler(), DecisionTreeClassifier(random_state=2, max_depth=1000))#make_pipeline_imb(SMOTE(),AdaBoostClassifier())#AdaBoostClassifier()#DecisionTreeClassifier(random_state=2, max_depth=1000)#AdaBoostClassifier()#svm.SVC(gamma='auto')#RandomForestClassifier()#GaussianNB()#RandomForestClassifier(n_estimators=100, max_depth=2,random_state=0)#LogisticRegression()#Model(inputs=inputs,outputs=preds)#DecisionTreeClassifier(criterion="entropy", max_depth=step_size, min_samples_split=2,min_samples_leaf=500)#Model(inputs=inputs,outputs=preds)
        #pipeline = model#KerasClassifier(build_fn=model, epochs=100, batch_size=5, verbose=0)
        return pipeline


    def create_model(self, step_size, nb_features):
        #This function will create the neural network model
        #Might need to take as input the corresponding training set to set the size
        #will return the model object
        inputs = Input(shape=(1,step_size))
        #if step_size > 1:
        #    step_size = step_size/2
        #preds = Dense(int(round(step_size)), activation='linear')(inputs)
        #preds = Dense(int(round(step_size)), activation='linear')(preds)
        #preds = Dense(64, activation='linear')(preds)
        #preds = Dense(4, activation='elu')(preds)
        #preds = Dense(2, activation='elu')(preds)
        #preds = LSTM(step_size, return_sequences=True)(inputs)
        #preds = Dense(step_size, activation='relu')(inputs)
        #preds = Dense(nb_features,activation='relu')(preds)
        preds = Dense(step_size, activation='linear')(inputs)
        preds = Dense(step_size, activation='linear')(preds)
        preds = Dense(nb_features, activation='sigmoid')(preds)
        #preds = Dense(nb_features, activation='softmax')(preds)
        #preds = Round()(preds)
        model = Model(inputs=inputs,outputs=preds)
        rms=Adam()#Adam()#RMSprop(lr=0.001) #Standard gradient descent (keep things simple)
        model.compile(loss='binary_crossentropy', optimizer=rms, metrics=['binary_accuracy'])#(loss='binary_crossentropy', optimizer=rms, metrics=['binary_accuracy'])
        pipeline = model#Pipeline([('standardize',round()), ('mlp',model)])
        #pipeline =  make_pipeline_imb(RandomOverSampler(), DecisionTreeClassifier(random_state=2, max_depth=1000))#make_pipeline_imb(SMOTE(),AdaBoostClassifier())#DecisionTreeClassifier(random_state=2, max_depth=1000)#AdaBoostClassifier()#svm.SVC(gamma='auto')#RandomForestClassifier()#GaussianNB()#RandomForestClassifier(n_estimators=100, max_depth=2,random_state=0)#LogisticRegression()#Model(inputs=inputs,outputs=preds)#DecisionTreeClassifier(criterion="entropy", max_depth=step_size, min_samples_split=2,min_samples_leaf=500)#Model(inputs=inputs,outputs=preds)
        #pipeline = model#KerasClassifier(build_fn=model, epochs=100, batch_size=5, verbose=0)
        return pipeline

    def train(self, X, Y, training_test_split):
        #This function will trigger the training of the neural network object
        #X is the input training dataset
        #Y are the target values
        #training_test_split is the split between the training and testing sets (60:20:20)
        step_size = X.shape[len(X.shape)-1] #number of input feature columns
        nb_features = Y.shape[len(Y.shape)-1] #number of target features


        if self.model == None:
            if len(X.shape) == 2:
                self.model = self.create_model2D(step_size, nb_features)
            else:
                self.model = self.create_model(step_size, nb_features)
            #rms=Adam()#RMSprop(lr=0.001) #Standard gradient descent (keep things simple)
            #self.model.compile(loss='binary_crossentropy', optimizer=rms, metrics=['binary_accuracy']) #Mean square error would allow us to determine how far the prediction is from the target values
        #self.printDistinct(Y[:training_test_split], training_test_split)
        #self.model = KerasClassifier(build_fn=self.create_model(step_size, nb_features), epochs=100, batch_size=5, verbose=0)
        hist = self.model.fit(X[:training_test_split], Y[:training_test_split])#, batch_size=100, epochs=10, verbose=1, validation_split=0.25) #Still need to identify the hyperperamters for the training such as batch_size, epochs
        return self.model

    def predict(self, X_test):
        #This function will use X_test to predict the target values
        #It will return the target values to be used for comparison with actual target values
        predicted = self.model.predict(X_test)
        return predicted
