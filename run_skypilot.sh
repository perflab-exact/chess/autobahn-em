#!/bin/bash
#SBATCH --time=64:15:00

module load java/20.0 
module load singularity/3.9.7
module load awscli/2.0

param2='s3://nextflow-bucket-s3/data/' 
param3='/qfs/projects/chess/leeh736/autobahn-em/patch/data' 
param4='results'
param5='/qfs/projects/chess/leeh736/autobahn-em/modules/compress-sky/compress_sky.yaml'
param6='/qfs/projects/chess/leeh736/autobahn-em/modules/'

    ./nextflow -log tmp/custom.log run main-sky.nf -with-timeline -with-report -with-trace -with-tower -profile slurm -bucket-dir 's3://nextflow-bucket-s3' --dataloc $param2 --datadir $param3 --outdir $param4 -ansi-log false --skypilot_compress_yaml_path $param5 --skypilot_modules_path $param6
