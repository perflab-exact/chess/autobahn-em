#!/usr/bin/env nextflow 

/*
 * Copyright (c) 2022, Seqera Labs.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * This Source Code Form is "Incompatible With Secondary Licenses", as
 * defined by the Mozilla Public License, v. 2.0.
 *
 */
import groovy.json.JsonSlurper


include { analyze_h5 } from './modules/analyze_prism'
include { generate_xyz } from './modules/prism'
include { generate_h5 } from './modules/prism'
include { generate_xyz_sky } from './modules/prism-sky'
include { generate_h5_sky } from './modules/prism-sky'
include { analyze_h5_sky } from './modules/prism-sky'
include { chip_sel } from './modules/ensemble'
include { ensemble_model } from './modules/ensemble'
include { train } from './modules/FENet'
include { compress_sky } from './modules/compress-sky'
include { compress} from './modules/compress'
include { uploadImagesAWS } from './modules/upload'
include { removeImagesAWS } from './modules/upload'
include { SAMIAM } from './modules/SamIAM'
include { SkyScience } from './modules/SkyScience'

nextflow.preview.recursion=true
nextflow.enable.dsl=2
log.info """
    M L - E X A M P L E   P I P E L I N E
    =====================================
    dataloc         : ${params.dataloc}
    datadir         : ${params.datadir}
    outdir          : ${params.outdir}
    """



/* 
 * main script flow SrTiO3
 */

def ch = Channel.value([1,2])

workflow stage_images {
    take:
    rr
    directory
    image_ID
    bucket

    main:
    uploadImagesAWS(rr, directory,image_ID, bucket)
 
    emit:
    uploadImagesAWS.out
}

workflow remove_images {
    take:
    rr
    bucket

    main:
    removeImagesAWS(rr, bucket)
}

workflow PRISM {
    take:
    chan
    cc
    rr
    API
    

    main:
    def dd = 0
    (x,y) = generate_xyz(chan, API, cc, rr)
    (w,nn) = generate_h5(chan, params.dataloc, x, y)
    (dd) = analyze_h5(chan, w, params.dataloc, x, nn, dd)
        
    emit:
    analyze_h5.out
}

workflow PRISM_sky {
    take:
    chan
    cc
    rr
    API
    

    main:
    def dd = 0
    (x,y) = generate_xyz_sky(chan, API, cc, rr)
        
}



workflow classification {
    take:
    out
 
    main:
    Channel.of("LiAl", "SrTiOGe").set{clas1}
    (r, name) = compress_sky(out, clas1)
    train(r, name)
     
}

process get_time {
    input:
    val(start)
    val(end)

    script:
    """
    #!/bin/bash
    echo ${start}
    echo ${end}
    """

}

workflow all {
    (result) = SkyScience('test run')

    def round = 1
    def r=""
    Channel.of(1..4).set{chan}
    Channel.of(["Li,Al", "LiAl", "0"],["Sr,Ti,O,Ge", "SrTiOGe", "0"], ["Li,Al", "LiAl", "45"],["Sr,Ti,O,Ge", "SrTiOGe", "45"]).set{cc}
    def API="r8MGCmFV1NLtxZ1jNTQ1evr31dXI1C8R"

    (oo) = PRISM(chan, cc , round, API)

    Channel.of("SrTiOGe").set{clas1}
    (r, name) = compress_sky(round, clas1)

    Channel.of("LiAl", "STO_GE_2").set{ID}
    Channel.of("background,precipitate", "STO,PtC,GE").set{mol}
    Channel.of("png", "jpg").set{ext}
    (chips, ww, mm, ext2) = chip_sel(round, ID, mol, ext)
    (rr) = ensemble_model(chips, ww, mm, round, ext2)

    SAMIAM(r, ext, ID)
    
}

workflow {
    (result) = SkyScience('test run')

    def round = 1
    def r=""
    /*Channel.of(1..4).set{chan}
    Channel.of(["Li,Al", "LiAl", "0"],["Sr,Ti,O,Ge", "SrTiOGe", "0"], ["Li,Al", "LiAl", "45"],["Sr,Ti,O,Ge", "SrTiOGe", "45"]).set{cc}*/
    Channel.of(["Sr,Ti,O,Ge", "SrTiOGe", "45"]).set{cc}
    Channel.of(1).set{chan}
    def API="r8MGCmFV1NLtxZ1jNTQ1evr31dXI1C8R"

    (oo) = PRISM_sky(chan, cc , round, API)


    Channel.of("SrTiOGe").set{clas1}
    /*(r, name) = compress_sky(round, clas1)*/

}

/* 
 * completion handler
 */
workflow.onComplete {
        log.info ( "$workflow.duration" )
	log.info ( workflow.success ? '\nDone!' : '\nOops .. something went wrong' )
}
