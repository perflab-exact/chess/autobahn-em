#!/bin/bash
export PATH=/share/apps/python/miniconda4.12/bin:/share/apps/gcc/9.1.0/bin:/people/scicons/deception/bin:/qfs/people/abeb563/.vscode-server/bin/b3e4e68a0bc097f0ae7907b217c1119af9e03435/bin/remote-cli:/people/scicons/deception/bin:/share/apps/python/miniconda4.12/condabin:/usr/lib64/qt-3.3/bin:/people/scicons/deception/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/opt/ganglia/bin:/opt/ganglia/sbin:/opt/pdsh/bin:/opt/rocks/bin:/opt/rocks/sbin:/people/abeb563/bin:/people/abeb563/bin:/people/abeb563/.local/bin

module load python/miniconda3.8
module load cuda/11.1

source /share/apps/python/miniconda4.12/etc/profile.d/conda.sh
pip install ninja==1.10.0
pip install torch==1.7.1
pip install torch-encoding==1.2.1
pip install torchvision==0.8.2
pip install protobuf==3.20.*
pip install mlflow --upgrade #==1.16.0
pip install barbar==0.2.1
pip install numpy==1.20.3
pip install pillow==8.2.0


python main.py --model=FENet --dataset=PRISM_HAADF --gamma=.99 --num_runs=1 --num_epochs=1
#python encoder.py --model=FENet