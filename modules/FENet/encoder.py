from opts import parse_opts
from Info import Datasets_Info
from prepare_dataloader import Prepare_DataLoaders
from utils import *
import torch.nn as nn
import torch
from train import train_model
from test import test_model
import mlflow
from torch.backends import cudnn
from datetime import datetime
import pickle



# cudnn.benchmark = True            # if benchmark=True for accelerating, deterministic will be False
# cudnn.deterministic = False

print(cudnn.benchmark)
print(cudnn.deterministic)

if __name__ == '__main__':
    opt_Parser = parse_opts()
    params_dict = vars(opt_Parser.parse_args())
    #logfile name
    now = datetime.now()
    timestamp = now.strftime("%Y-%m-%d__%H-%M-%S/")
    log_dir = os.path.join(params_dict['dir_log'], params_dict['scope'], params_dict['dataset'],
                               params_dict['model'] + '_' + params_dict['backbone'][-2:] + '_' + params_dict['save_name'] + '_' + timestamp)
    os.makedirs(os.path.dirname(log_dir),exist_ok=True)
    #setup log_file
    logfile = f"output.log"
    logger = get_logger(log_dir,logfile)
    opt_Parser = Parser(opt_Parser,logger=logger,log_dir=log_dir)
    opt = opt_Parser.get_arguments()
    num_Runs = Datasets_Info['splits'][opt.dataset]
    num_Runs = opt.num_runs if opt.num_runs else num_Runs
    opt.n_classes = Datasets_Info['num_classes'][opt.dataset]

    if opt.gpu_ids and torch.cuda.is_available():
        device = torch.device("cuda:%d" % opt.gpu_ids[0])
        torch.cuda.set_device(opt.gpu_ids[0])
        torch.cuda.manual_seed(opt.seed)
    else:
        device = torch.device("cpu")
        torch.manual_seed(opt.seed)
    

    model = initialize_model(opt).to(device)

    del model.UP
    model.fc = model.fc[:-2]

    print(model)