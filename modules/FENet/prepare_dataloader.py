## PyTorch dependencies
import torch
from torchvision import transforms
from torchvision.datasets import ImageFolder
#from torchvision.datasets import DTD
from datasets.DTD import DTD
from Info import Datasets_Info
from utils import *
import os
from datasets.GTOS_mobile import GTOS_mobile_single_data

def Prepare_DataLoaders(opt, split, input_size=(224,224)):
    
    dataset = opt.dataset
    data_dir = Datasets_Info['data_dirs'][dataset]

    train_data_transforms_list = [transforms.Resize(opt.resize_size),
                                transforms.RandomResizedCrop(input_size,scale=(.8,1.0)),
                                transforms.RandomHorizontalFlip(),
                                transforms.ToTensor(),
                                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])]

    test_data_transforms_list = [transforms.Resize(opt.center_size),
                                transforms.CenterCrop(input_size),
                                transforms.ToTensor(),
                                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])]

    if opt.rotation_need:
        test_data_transforms_list.insert(2, transforms.RandomAffine(opt.degree))


    data_transforms = {'train':transforms.Compose(train_data_transforms_list), 'test':transforms.Compose(test_data_transforms_list)}

    train_dataset, val_dataset, test_dataset = None, None, None

    # Create training and test datasets
    if dataset == 'GTOS-mobile':
        # Create training and test datasets
        train_dataset = GTOS_mobile_single_data(data_dir, kind = 'train',
                                           image_size=opt.resize_size,
                                           img_transform=data_transforms['train'])

        test_dataset = GTOS_mobile_single_data(data_dir, kind = 'test',
                                           img_transform=data_transforms['test'])
    
    elif dataset == 'DTD':
       transform = transforms.Compose([
            transforms.Resize((224, 224)),  # Resize the images to a consistent size
            transforms.ToTensor(),  # Convert the images to tensors
            transforms.Normalize(mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5])  # Normalize the image tensors
        ])
       #train_dataset = DTD('./data/download/','train',transform=transform,download=True)
       #test_dataset = DTD('./data/download/','test',transform=transform,download=True)

       train_dataset = DTD('./data/download/',split='train',transform=transform)
       test_dataset = DTD('./data/download/',split='test',transform=transform)

       print("train_dataset",len(train_dataset))
       print("test_dataset",len(test_dataset))
    
    elif dataset == "STO_GE_2":
        transform = transforms.Compose([
        transforms.Resize(224),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        ])

        # Load the training dataset
        train_dataset = ImageFolder(root=f"./data/STO_GE_2_dataset/train", transform=transform)
        # Load the validation dataset
        val_dataset = ImageFolder(root=f"./data/STO_GE_2_dataset/val", transform=transform)
        # Load the test dataset
        #test_dataset = ImageFolder(root=f"./data/STO_GE_2_dataset/test", transform=transform)
    
    elif dataset == "PRISM":
        transform = transforms.Compose([
        transforms.Resize(224),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        ])

        # Load the training dataset
        train_dataset = ImageFolder(root=f"./data/PRISM/train", transform=transform)
        # Load the validation dataset
        val_dataset = ImageFolder(root=f"./data/PRISM/val", transform=transform)
        # Load the test dataset
        test_dataset = ImageFolder(root=f"./data/PRISM/test", transform=transform)
    
    elif dataset == "PRISM_HAADF":
        transform = transforms.Compose([
        transforms.Resize(224),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        ])

        # Load the training dataset
        train_dataset = ImageFolder(root=f"./data/PRISM_HAADF/train", transform=transform)
        # Load the validation dataset
        val_dataset = ImageFolder(root=f"./data/PRISM_HAADF/val", transform=transform)
        # Load the test dataset
        test_dataset = ImageFolder(root=f"./data/PRISM_HAADF/test", transform=transform)

    image_datasets = {'train': train_dataset, 'val': val_dataset, 'test':test_dataset}

    # Create training and test dataloaders
    dataloaders_dict = {x: torch.utils.data.DataLoader(image_datasets[x], 
                                                       batch_size=eval('opt.{}_BS'.format(x)), 
                                                       shuffle=True if x=='train' else False,
                                                       num_workers=opt.num_workers,
                                                       pin_memory=opt.pin_memory) for x in ['train', 'val', 'test']}
    
    return dataloaders_dict


