process massivegnn_sky {
    label 'local_launch'

    input:
    val(rr)
    val(start)

    output:
    env (done), emit: x

    """
    sky launch --env INPUT="$start" -c massiveGNN_cluster_for_${start} -y ${params.skypilot_modules_path}/massiveGNN-sky/massivegnn_sky.yaml
    export done=1
    sky down massiveGNN_cluster_for_${start} -y
    """
}
