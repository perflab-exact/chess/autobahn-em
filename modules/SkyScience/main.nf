process SkyScience {

    label = 'local_launch'

    input:
    val(input_text)

    script:
    """
    #!/bin/bash

    hostname
    echo ${input_text}
    # setup
    # 2m 13s on bluesky
    pip install "skypilot-nightly[aws]"
    """

}
