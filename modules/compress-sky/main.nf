process compress_sky {
    label 'local_launch'

    input:
    val(rr)
    val(start)

    output:
    env (done), emit: x
    env (comp_name), emit: y

    """
    sky launch --env INPUT="$start" -c compress_cluster_for_${start} -y $params.skypilot_compress_yaml_path
    export done=1
    export comp_name=${start}_comp
    sky down compress_cluster_for_${start} -y
    """
}
