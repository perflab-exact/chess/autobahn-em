process generate_h5_sky {
    label 'local_launch'
    stageInMode "run_prism.py", mode: "copy"

    input:
    val(x)
    val(bucketID)
    val(data_loc)
    val(temp)

    output:

    """
    sky launch -c prism_cluster -y --use-spot  $params.skypilot_modules_path/prism-sky/prism_generate_h5_sky.yaml
    """
}

process generate_xyz_sky {
    label 'local_launch'
    
    stageInMode "gen.py", mode: "copy"
    stageInMode "materials_project_api.yaml", mode: "copy"

    input:
    val(d)
    val(MP_API_KEY)
    tuple val(molID), val(matID), val(ang)
    val(temp)
       
    output:

    """
    sky launch --env molID="$molID" --env matID="$matID" --env ang="$ang" -c prism_cluster -y --use-spot  $params.skypilot_modules_path/prism-sky/prism_generate_xyz_sky.yaml
    """
}

process analyze_h5_sky {
    label 'local_launch'
    stageInMode "example.py", mode: "copy"

    input:
    val(rr)
    val(reads)
    val(bucketID)
    val(temp)
    val(start)
    val(done)
       
    output:

    """
    sky launch -c prism_cluster -y --use-spot  $params.skypilot_modules_path/prism-sky/prism_analyze_h5_sky.yaml
    sky down prism_cluster -y
    """
}
