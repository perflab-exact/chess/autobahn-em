#!/usr/bin/env python
# coding: utf-8

import pyprismatic as pr
import os
import pandas as pd
import argparse

DIRNAME = os.path.dirname(__file__)
DEFAULT_DIR = os.path.join(DIRNAME, "..", "prism_output_files")


def set_meta_field(field:str, value, meta: pr.Metadata):

    # This is incorrectly cast as float
    # meta.earlyCPUStopCount = int(meta.earlyCPUStopCount)
    missed_int_fields = ['randomSeed', 'earlyCPUStopCount']

    if field in meta.str_fields:
        setattr(meta, field, value)

    elif field in meta.int_fields or field in missed_int_fields:
        setattr(meta, field, int(value))

    elif field in meta.float_fields:
        setattr(meta, field, float(value))

    else:
        bool_value = value == 'True'
        setattr(meta, field, bool(bool_value))
def read_prism_parameters(param_file: str,
                      meta: pr.Metadata,
                      param_combo_file: str = None,
                      param_combo_number: int = None):
    with open(param_file, 'r') as f:

        for line in f:

            line = line.strip()
            field, value = line.split(" = ")
            set_meta_field(field, value, meta)


    if param_combo_file:
        param_combo_df = pd.read_csv(param_combo_file, index_col='combo_id')
        combo = param_combo_df.loc[param_combo_number]

        param_names = list(combo.index)

        for field in param_names:
            value = combo[field]
            set_meta_field(field, value, meta)
def run_prism(fpath: str,
          param_file: str,
          output_dir: str = DEFAULT_DIR,
          output_name: str = None,
          param_combo_file: str = None,
          param_combo_number: int = None):
    if param_combo_file is None != param_combo_number is None:
        print("You must supply both a param_combo_file and param_combo_number")
        return

    fpath = os.path.abspath(fpath)
    bname = os.path.basename(fpath)

    if param_combo_number is not None:
        output_bname = f"{bname[:-4]}_combo_{param_combo_number}.h5"
    else:
        output_bname = f"{bname[:-4]}.h5"

    output_name = os.path.join(output_dir, output_bname)

    os.makedirs(output_dir, exist_ok=True)

    meta = pr.Metadata(filenameAtoms=fpath)
    meta.filenameOutput = output_name

    read_prism_parameters(param_file, meta, param_combo_file=param_combo_file,
                      param_combo_number=param_combo_number)

    meta.go()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog = 'run_prism',
                description = 'Run PRISM software via python')

    parser.add_argument('fpath',
                help = 'Path to PRISM input file' )

    parser.add_argument('param_file',
                help = 'Path to PRISM parameter file')

    parser.add_argument('-d', '--output_dir', default=DEFAULT_DIR,
                help = 'Output directory to store PRISM *.h5 file')

    parser.add_argument('-n', '--output_name', default=None,
                help = ' Name of output .h5 file. Defualt is Same name as PRSIM input file with the \
                        *.xyz extension replaced with *.h5')

    parser.add_argument('--param_combo_file', default=None,
                help = 'Parameter combination file to use')

    parser.add_argument('--param_combo_number', default=None,
                help = 'Parameter combination number from param_combo_file')

    args = parser.parse_args()

    fpath       = args.fpath
    param_file  = args.param_file
    output_dir  = args.output_dir
    output_name = args.output_name
    param_combo_file = args.param_combo_file
    param_combo_number = args.param_combo_number

    if param_combo_number:
        param_combo_number = int(param_combo_number)


    run_prism(fpath, param_file, output_dir=output_dir, output_name=output_name,
              param_combo_file=param_combo_file, param_combo_number=param_combo_number)
