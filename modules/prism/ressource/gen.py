# from pymatgen.ext.matproj import MPRester
# from mp_api.matproj import MPRester
from mp_api.client import MPRester
# from pymatgen import MPRester
import pymatgen
import pymatgen.io.prismatic
import os
# MP_API_KEY=os.environ.get("MP_API_KEY")
import yaml
import argparse
from pymatgen.core.operations import SymmOp
import requests
# from mp_api.client import MPRester

DIRNAME = os.path.dirname(__file__)
DEFAULT_DIR = os.path.join(DIRNAME, "..", "prism_input_files")
MATERIALS_INFO_FILE = os.path.join(DIRNAME, "..", "materials_map.yml")

def get_api_key(api_file=os.path.join(DIRNAME,"materials_project_api.yaml")):
    with open(api_file, 'r') as f:
        d = yaml.safe_load(f)

    api_key = d['api_key']
    return api_key

def retrieve_material(mat_id: str,
                      API_KEY: str):

    print(API_KEY)
    print("===========================================")
    mpr = MPRester(API_KEY)

    # This returns a list of dictionaries, our query has one entry
    print(list(mat_id.split(",")))
    query_list = mpr.summary.search(elements=list(mat_id.split(",")))
    # query_list = mpr.summary.search(material_ids=[mat_id])
    query = query_list[0]
    print(query)

    return query
def mp_to_prismatic(mat_id, rotation_axis: list[int] = [1, 0, 0], angle: int = 0,
                prism_file_dir: str = DEFAULT_DIR):

    # Retrieve api key
    api_key = get_api_key()

    query = retrieve_material(mat_id, api_key)

    # Get the pretty chemical formula
    pretty_formula = query.formula_pretty # python string

    # Get the strucutre details
    structure = query.structure # pymatgen strucutre class

    # Symmetry information
    symmetry = query.symmetry

    # Save material information
    print(symmetry)
    material_dict = {}
    d = {}
    d['pretty_formula'] = pretty_formula
    d['symmetry_symbol'] = symmetry.symbol
    d['crystal_sytem'] = f'{symmetry.crystal_system}'
    mat_id = mat_id.replace(",", "")
    material_dict[mat_id] = d

    existing_info: dict = {}
    if os.path.exists(MATERIALS_INFO_FILE):
        with open(MATERIALS_INFO_FILE, 'r') as f:
            existing_info = yaml.safe_load(f)

    existing_info.update(material_dict)
    with open(MATERIALS_INFO_FILE, 'w') as f:
        yaml.dump(existing_info, f)

    # Apply rotation
    operation = SymmOp.from_axis_angle_and_translation(rotation_axis, angle)
    structure.apply_operation(operation)

    # Save strucutre as prism inptu file
    rot_str = f"{rotation_axis[0]}_{rotation_axis[1]}_{rotation_axis[2]}_angle_{angle}"
    prism_file_name = f"{mat_id}_{rot_str}.xyz"
    prism_file = os.path.join(prism_file_dir, prism_file_name)
    ff = os.path.join(prism_file_dir, "f_name.txt")

    prismatic_str = pymatgen.io.prismatic.Prismatic(structure).to_string()
    os.makedirs(prism_file_dir, exist_ok=True)

    with open(prism_file, 'w') as f:
        f.write(prismatic_str)
    with open(ff, 'w') as f:
        f.write(pretty_formula)
if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog = 'mp_to_prismatic',
                description = 'Retrive material from Materiasl Project, apply rotations, and save structure as PRISM input file')

    parser.add_argument('mat_id',
                help = 'Materials project ID' )

    parser.add_argument('-r', '--rotation_axis', default='100',
                help = 'Axis to rotate material (in format XYZ)')

    parser.add_argument('-a', '--angle', default=0,
                help = 'Angle (in degrees) to rotate materials')

    parser.add_argument('-d', '--prism_file_dir', default="prism_input_files",
                help = 'Directory to store generated prism input file. Default'
                ' is prism_input_files')

    args = parser.parse_args()

    mat_id       = args.mat_id
    rotation_axis  = args.rotation_axis
    angle  = int(args.angle)
    prism_file_dir = args.prism_file_dir

    # Convert rotation axis to list
    ra_list = [int(a) for a in rotation_axis]

    mp_to_prismatic(mat_id, rotation_axis=ra_list, angle=angle, prism_file_dir=prism_file_dir)
