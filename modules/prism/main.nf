process generate_h5 {
    label 'long'
    def start=""
    def nn=""
    def start1=""
    def end=""
 
    input:
    val(x)
    val(bucketID)
    val(data_loc)
    val(temp)    

    output:
    env(start), emit: w
    env(nn), emit: q

    script:
    """
    #!/bin/bash
    cat << EOF >> materials_project_api.yaml
    api_key: "r8MGCmFV1NLtxZ1jNTQ1evr31dXI1C8R"
    EOF
    cat << EOF >> environment.yml
    name: prism
    channels:
      - conda-forge
    dependencies:
      - python=3.10
      - jupyterlab
      - matplotlib
      - prismatic
      - pymatgen
      - pip
      - pip:
        - py4dstem
        - mp_api
    EOF

    export PATH=/opt/conda/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:$PATH
    source  /opt/conda/etc/profile.d/conda.sh
    conda install -y -n base -c conda-forge pymatgen prismatic python==3.10 pip 
    conda create -n aws
    conda activate aws

    ls /app
    ls /
    cp -r /home/ec2-user/* .
    ls
    mv run_prism.py prism/
    ls prism/* 

    if [ -f "prism_input_params_example.txt" ]; then
        echo "prism_input_params_example.txt exists"
    else
        start1=`date +%s%N`
        aws s3 cp s3://nextflow-bucket-s3/data/prism_input_params_example.txt .
        end=`date +%s%N`
        runtime=\$((end-start1))
        FILESIZE=\$(stat -c%s "prism_input_params_example.txt")
        aws s3 cp s3://nextflow-bucket-s3/data/downloadS3EC2.txt .
        flock -x downloadS3EC2.txt
        echo "\$FILESIZE \$runtime" >> downloadS3EC2.txt
        aws s3 cp downloadS3EC2.txt s3://nextflow-bucket-s3/data/ 
        flock -u downloadS3EC2.txt
    fi
    cd prism_input_files
    if [ -f "${data_loc}"]; then
        echo "${data_loc} exists"
    else
        aws s3 cp s3://nextflow-bucket-s3/data/${data_loc} .
    fi
    cd ..
    ls prism_input_files
    cp prism_input_params_example.txt templates
    mv materials_project_api.yaml prism
    chmod 600 prism/materials_project_api.yaml

    apt-get update
    apt-get install -y wget

    echo "00000000000000000000000000000000"


    pip install --user mpcontribs-client
    pip install --user mp_api
    pip install --user py4dstem
    pwd
    python prism/run_prism.py prism_input_files/${data_loc} templates/prism_input_params_example.txt
    start1=`date +%s%N`
    aws s3 cp prism_output_files/ s3://nextflow-bucket-s3/data/ --recursive
    end=`date +%s%N`

    runtime=\$((end-start1))
    FILESIZE=\$(du -sh  prism_output_files/ | awk '{ print \$1 }') 

    aws s3 cp s3://nextflow-bucket-s3/data/downloadEC2S3Large.txt .
    flock -x downloadEC2S3Large.txt
    echo "\$FILESIZE \$runtime" >> downloadEC2S3Large.txt
    aws s3 cp downloadEC2S3Large.txt s3://nextflow-bucket-s3/data/
    flock -u downloadEC2S3Large.txt   

    export start=`basename ${data_loc} .xyz`
    export nn=${temp}
    """
}
process generate_xyz {
    label 'long'
    def path_to_file=""
    def pretty_eq=""
    def start=""
    def end=""
    def runtime=""
    def FILESIZE=""
    input:
    val(d)
    val(MP_API_KEY)
    tuple val(molID), val(matID), val(ang)
    val(temp)

    output:
    env (path_to_file), emit: x
    env (pretty_eq), emit: y   

    script:
    """
    #!/bin/bash   
    df -h
    export PATH=/opt/conda/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:$PATH

    which conda
    which gcc

    echo "00000000000000000000000000000000"
    source  /opt/conda/etc/profile.d/conda.sh
    conda create -n aws
    conda activate aws

    ls /app
    ls /
    cp -r /home/ec2-user/* .
    ls
    mv gen.py prism/
    ls prism/*

 
    cat << EOF >> materials_project_api.yaml
    api_key: "${MP_API_KEY}"
    EOF
    cat << EOF >> environment.yml
    name: prism
    channels:
      - conda-forge
    dependencies:
      - python=3.10
      - jupyterlab
      - matplotlib
      - prismatic
      - pymatgen
      - pip
      - pip:
        - py4dstem
        - mp_api
    EOF

    mv materials_project_api.yaml prism
    chmod 600 prism/materials_project_api.yaml

    pip install --user mpcontribs-client
    pip install --user mp_api
    pip install --user py4dstem
 
    python prism/gen.py ${molID} -r 100 -a ${ang}
    cd prism_input_files
    export path_to_file=`ls ${matID}*`
    export pretty_eq="${matID}" #`cat f_name.txt`
    cd ..
    start=`date +%s%N`
    aws s3 cp prism_input_files/ s3://nextflow-bucket-s3/data/ --recursive
    end=`date +%s%N`
    
    runtime=\$((end-start)) 
    FILESIZE=\$(du -sh --apparent-size  prism_input_files/ | awk '{ print \$1 }')

    aws s3 cp s3://nextflow-bucket-s3/data/downloadEC2S3.txt .
    flock -x downloadEC2S3.txt
    echo "\$FILESIZE \$runtime" >> downloadEC2S3.txt
    aws s3 cp downloadEC2S3.txt s3://nextflow-bucket-s3/data/
    flock -u downloadEC2S3.txt

    echo $path_to_file
    ls
    ls prism_input_files
    ls prism
    """
}
