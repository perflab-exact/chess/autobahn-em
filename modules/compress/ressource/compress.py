import os
from PIL import Image
compressai = "./CompressAI/examples/codec.py"
image_path = "./image_to_compress" #"/qfs/projects/oddite/lenny/projects/compressai_profiling/TIFF1"
decomp_image_path = "./decompress_images"
result_path = "./result.txt"

def create_directory(directory_path):
    if not os.path.exists(directory_path):
        os.makedirs(directory_path)
        print(f"Directory '{directory_path}' created.")
    else:
        print(f"Directory '{directory_path}' already exists.")


def run_test(directory, decomp_directory, extensions=['.jpg', '.jpeg', '.png', '.gif', '.tiff']):
    create_directory(decomp_directory)

    for root, dirs, files in os.walk(directory):
        for file in files:
            if os.path.splitext(file)[1].lower() in extensions:
                img_path = os.path.join(root, file)
                for quality in range(1, 9, 1):
                    with Image.open(f'{img_path}') as img:
                        width, height = img.size

                    with open(result_path, 'a') as f:
                        f.write(f"image name: {img_path.split('/')[-1]} quality level: {quality} Height: {height} Width: {width}")

                    img_name = img_path.split('/')[-1]
                    img_name = img_name.split('.')[0]
                    print(f"python -u " + compressai + " encode " + img_path + " -q "+ str(quality)+" --cuda -o "+decomp_directory+"/"+img_name+"."+str(quality)+".compressai >> "+result_path)
                    os.system(f"python -u " + compressai + " encode " + img_path + " -q "+str(quality)+" --cuda -o "+decomp_directory+"/"+img_name+"."+str(quality)+".compressai >> "+result_path)


run_test(image_path, decomp_image_path)
