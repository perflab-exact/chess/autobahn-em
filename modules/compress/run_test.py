
import os
from PIL import Image

# this is the directory of the compressai codec.py
compressai = "./compressai/examples/codec.py"

# this is the directory of the dataset
image_path = "/qfs/projects/oddite/lenny/projects/compressai_profiling/TIFF"

# this is the directory of the decompressed image
decomp_image_path = "/qfs/projects/oddite/lenny/projects/compressai_profiling/decompress_images"

# this is the directory of the result
result_path = "./result.txt"

def create_directory(directory_path):
    if not os.path.exists(directory_path):
        os.makedirs(directory_path)
        print(f"Directory '{directory_path}' created.")
    else:
        print(f"Directory '{directory_path}' already exists.")


def run_test(directory, decomp_directory, extensions=['.jpg', '.jpeg', '.png', '.gif', '.tiff']):
    """
    Recursively finds all images in a directory. And then compress and decompress with compressAI with different quality.

    :param directory: The directory to search.
    :param decomp_directory: The directory to store decompressed data.
    :param extensions: A list of file extensions to search for.
    """
    create_directory(decomp_directory)

    for root, dirs, files in os.walk(directory):
        for file in files:
            if os.path.splitext(file)[1].lower() in extensions:
                img_path = os.path.join(root, file)
                for quality in range(1, 9, 1):
                    with Image.open(f'{img_path}') as img:
                        width, height = img.size

                    with open(result_path, 'a') as f:
                        f.write(f"image name: {img_path.split('/')[-1]} quality level: {quality}\nHeight: {height} Width: {width}\n")

                    img_name = img_path.split('/')[-1]
                    img_name = img_name.split('.')[0]
                    print(f"python -u {compressai} encode {img_path} -q {quality} --cuda -o {decomp_directory}{img_name}.{quality}.compressai >> {result_path}")
                    print(f"python -u {compressai} decode {decomp_directory}{img_name}.{quality}.compressai --cuda -o {decomp_directory}{img_name}_decomp_{quality}.png >> {result_path}")
                    os.system(f"python -u {compressai} encode {img_path} -q {quality} --cuda -o {decomp_directory}{img_name}.{quality}.compressai >> {result_path}")
                    os.system(f"python -u {compressai} decode {decomp_directory}{img_name}.{quality}.compressai --cuda -o {decomp_directory}{img_name}_decomp_{quality}.png >> {result_path}")


run_test(image_path, decomp_image_path)