from PIL import Image
import h5py
import numpy as np
from pathlib import Path

for hdf5 in Path("/qfs/projects/chess/data").glob("**/*.hdf5"):
    print(hdf5)
    f = h5py.File(hdf5)
    for a in f.keys():
        for b in f[a].keys():
            for c in f[a][b].keys():
                fileName = f"{a}_{b}_{c.replace('.dm4', '.tiff')}"
                if Path(fileName).exists():
                    continue
                if "image" not in f[a][b][c].keys():
                    continue
                img = Image.fromarray(np.array(f[a][b][c]['image']))
                img.save(fileName)
