import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from accuracy_winnow.get_accuracy import get_acc, get_best_model, get_feat, get_val, sort_feat, get_scalar, create_heatmap, MyEnsemble, get_test_data, format_output, MyEnsemble2, get_conf_mat
from few_shot.resnet import resnet18, resnet34, resnet50, resnet101, resnet152
import torchvision.models as models
from few_shot.protonet import PrototypicalNet
import glob
import shutil
from few_shot.pychip_classifier import PychipClassifier
from IPython.display import Image, display, Markdown
import sys

def isfloat(num):
    try:
        float(num)
        return True
    except ValueError:
        return False

input_chips = sys.argv[4]
input_chips = input_chips.split("|")
support_dict = {}
for x in input_chips[:-1]:
    temp = x.split(":")
    support_dict[temp[0]] = temp[1].split(",")
print(support_dict)

data_path = "./data/" + sys.argv[1]
img_name = sys.argv[1] + "." + sys.argv[2]
results_path = "./testing"
chips_path = "./testing/chips"


import torch
from torch.nn import Softmax
import torch.optim as optim
import math
count = 0
n_columns = 128 #64 #128 #50 #64 #50 #128
n_rows = 96 #48 #96 #50 #48 #50 #96
sive = 2048

feat = get_feat("data/"+sys.argv[1]+"/"+sys.argv[1]+"_truth_df.csv")
val = get_val("data/"+sys.argv[1]+"/"+sys.argv[1]+"_truth_df.csv")

ff = sys.argv[3]
ff = ff.split(",")
dd = {}
count = 0
for i in ff:
    dd[i] = "set_"+str(count)+"_label"
    count += 1

new_feat = []
new_val = []
for x in range(len(feat)):
    new_feat.append(feat[x])
    if val[x] in dd.keys():
        new_val.append(val[x])
val = new_val
feat = new_feat

l2 = {}
for x in range(len(val)):
    if feat[x] not in l2.keys():
        l2[feat[x]] = [dd[val[x]]]
    else:
        l2[feat[x]].append(dd[val[x]])

l = {}
for x in range(len(val)):
    if feat[x] not in l.keys():
        l[feat[x]] = [val[x]]
    else:
        l[feat[x]].append(val[x])

ss = {}
for i in support_dict.keys():
    ss[dd[i]] = support_dict[i][:30]
support_dict = ss

classifier = PychipClassifier(data_path, img_name)
classifier.preprocess("CLAHE", clipLimit=2.0)
classifier.chips_genesis(n_rows, n_columns)
print(support_dict)
classifier.support_genesis(support_dict=support_dict)

res18 = resnet18(pretrained=True, place_on_device=False)
res34 = resnet34(pretrained=True, place_on_device=False)
res50 = resnet50(pretrained=True, place_on_device=False)
res101 = resnet101(pretrained=True, place_on_device=False)
res152 = resnet152(pretrained=True, place_on_device=False)
res101V2 = models.resnet101(pretrained=True)
shuffle = models.shufflenet_v2_x1_0(pretrained=True)

Ensemble = [res18,res34, res50, res101, res152] #, res101V2, shuffle]
encoder_list = [ "resnet18", "resnet34", "resnet50", "resnet101", "resnet152"] #, "torch101", "shufflenet"]

full_model = {}
fname = []
for i in range(len(encoder_list)):
    model = PrototypicalNet(encoder=Ensemble[i], device='cpu')
    classifier.predict(savepath=results_path, encoder=encoder_list[i], seed=75, filename='results_tt_'+encoder_list[i]+'.csv')
    get_acc("tt_"+encoder_list[i], "accuracy_winnow/acc_temp_res.csv", i, "data/"+sys.argv[1]+"/"+sys.argv[1]+"_truth_df.csv", support_dict, dd)
    temp = classifier.results.to_numpy()
    if i == 0:
        fname = classifier.results["chip"].to_numpy()
    tt = temp[:,:len(list(support_dict.keys()))]
    for j in range(tt.shape[1]):
        if j not in full_model.keys():
            full_model[j] = []
        full_model[j].append(tt[:,j])

f = open("accuracy_winnow/acc_temp_res.csv", "r")
data = f.readlines()
f.close()
t = {}
acc_val = []
for i in data:
    temp = i.split(",")
    print(temp)
    t[float(temp[1])] = temp[0]
    acc_val.append(float(temp[1]))
print(acc_val)

print(full_model)

for i in full_model.keys():
    t = np.array(full_model[i])
    temp = []
    print(t.shape)
    for x in range(t.shape[1]):
        sum_weighted = 0
        count = 0
        for j in t[:,x]:
            if isfloat(j):
                sum_weighted += math.exp(acc_val[count]*100)*j
                count = count +1
        temp.append(sum_weighted/len(t[:,x]))
    full_model[i] = np.array(temp)

total = []
for i in full_model.keys():
    total.append(full_model[i])
total = np.array(total).T

print(total)
print(total.shape)
print(list(support_dict.keys()))

final_df = pd.DataFrame(total, columns = list(support_dict.keys())) #["set_3_label", "particle", "set_2_label"])
final_df.index = fname
final_df['prediction'] = final_df.idxmax(axis=1)
final_df['chip'] = final_df.index
print(final_df)
filepath = os.path.join("testing", 'results_ensemble.csv')
final_df.to_csv(filepath)

with torch.no_grad():
    get_scalar(Ensemble, classifier.support_set, classifier.query_set, 0, feat, val, dd)

get_acc("ensemble", "accuracy_winnow/acc_res.csv", 0, "data/"+sys.argv[1]+"/"+sys.argv[1]+"_truth_df.csv", support_dict, dd)
get_conf_mat("ensemble", "data/"+sys.argv[1]+"/"+sys.argv[1]+"_truth_df.csv", dd)
