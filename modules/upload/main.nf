process uploadImagesAWS {
    label 'short'

    input:
    val(rr) 
    val(directory)
    val(imageID)
    val(bucket)

    output:
    val (rr), emit: x

    script:
    """
    #!/bin/bash

    ls ${directory}
    which aws
    aws --version
    mkdir tmp
    touch tmp/README
    aws s3 sync tmp ${bucket}tmp/ --recursive
    aws s3 cp ${directory}/${imageID} ${bucket}tmp/ --recursive
    echo "---------------------------------------------------"
    ls ${directory}/${imageID}
    echo "-----------------------------------------------------"
    """
}

process removeImagesAWS {
    label 'short'

    input:
    val(rr)
    val(bucket)

    output:
    val (rr), emit: x

    script:
    """
    #!/bin/bash

    which aws
    aws --version
    aws s3 rm ${bucket}tmp/ --recursive
    echo "-----------------------------------------------------"
    """
}
